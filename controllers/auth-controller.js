const authService = require('../services/auth-service');

class AuthController {
    async generateQrCode(req, res, next) {
        try {
            const { userId } = req.body;
            const qrCode = await authService.generateQrCode(userId);

            return res.json({ qrCode });
        } catch (e) {
            next(e);
        }
    }

    async verifyAuthToken(req, res, next) {
        try {
            const { token, userId } = req.body;
            const authData = await authService.verifyAuthToken(token, userId);

            if (authData.verified) {
                res.cookie('verifyToken', authData.verifyToken, {maxAge: 30 * 24 * 60 * 60 * 1000, httpOnly: true, sameSite: 'none', secure: true });
            }

            return res.json({ verified: authData.verified });
        } catch (e) {
            next(e);
        }
    }
}


module.exports = new AuthController();