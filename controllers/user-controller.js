const userService = require('../services/user-service');
const { validationResult } = require('express-validator');
const ApiError = require('../exceptions/api-error');

class UserController {
    async registration(req, res, next) {
        try {
            const errors = validationResult(req);

            if (!errors.isEmpty()) {
                return next(ApiError.BadRequest('Ошибка при валидации', errors.array()));
            }

            const { email, password } = req.body;
            const userData = await userService.registration(email, password);

            const options = { 
                maxAge: 30 * 24 * 60 * 60 * 1000, 
                httpOnly: true, 
                sameSite: 'none', 
                secure: true 
            }

            res.cookie('refreshToken', userData.refreshToken, options);

            return res.json(userData);
        } catch (e) {
            next(e);
        }
    }

    async login(req, res, next) {
        try {
            const { email, password } = req.body;
            console.log(email, password);
            const userData = await userService.login(email, password);

            const options = { 
                maxAge: 30 * 24 * 60 * 60 * 1000, 
                httpOnly: true, 
                sameSite: 'none', 
                secure: true 
            }

            res.cookie('refreshToken', userData.refreshToken, options);

            return res.json(userData);
        } catch (e) {
            next(e);
        }
    }

    async logout(req, res, next) {
        try {
            const {refreshToken } = req.cookies;
            const token = await userService.logout(refreshToken);

            res.clearCookie('refreshToken');
            res.clearCookie('verifyToken');
            return res.json(token);
        } catch (e) {
            next(e);
        }
    }

    async activate(req, res, next) {
        try {
            const activationLink = req.params.link;
            await userService.activate(activationLink);

            return res.redirect(process.env.CLIENT_URL);
        } catch (e) {
            next(e);
        }
    }

    async refresh(req, res, next) {
        try {
            const { refreshToken, verifyToken } = req.cookies;
            const userData = await userService.refresh(refreshToken);

            if (userData.refreshToken) {
                const options = { 
                    maxAge: 30 * 24 * 60 * 60 * 1000, 
                    httpOnly: true, 
                    sameSite: 'none', 
                    secure: true 
                }

                res.cookie('refreshToken', userData.refreshToken, options);
            }
        
            return res.json({...userData, isVerify: !!verifyToken});
        } catch (e) {
            next(e);
        }
    }

    async getUsers(req, res, next) {
        try {
            const users = await userService.getAllUsers();
            return res.json(users);
        } catch (e) {
            next(e);
        }
    }
}


module.exports = new UserController();