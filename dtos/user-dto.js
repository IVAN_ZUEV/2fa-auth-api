module.exports = class UserDto {
    email;
    id;
    isActivated;
    is2FA;
    lastTokenRefreshTime;

    constructor(model) {
        this.email = model.email;
        this.id = model._id;
        this.isActivated = model.isActivated;
        this.is2FA = model.is2FA;
        this.lastTokenRefreshTime = model.lastTokenRefreshTime;
    }
}