const express = require('express');
const app = express();
const http = require('http');
const router = require('./routes/router');
const mongoose = require('mongoose');
const dotenv = require('dotenv');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const mongodb = require('mongodb');
const errorMiddleware = require('./middlewares/error-middleware');

dotenv.config();
app.use(bodyParser.json());
app.use(cookieParser());
app.use(
    cors({
        credentials: true,
        origin: process.env.CLIENT_URL
    })
);
app.use('/api', router);
app.use(errorMiddleware);

const port = process.env.PORT || 8000;
const server = http.createServer(app);

const start = async () => {
    try {
        await mongoose.connect(process.env.MONGO_URL);
        server.listen(port, () => {
            console.log(`Server is running on ${port} port`)
        })
    } catch (e) {
        console.log(e);
    }
}

start();

module.exports = app;