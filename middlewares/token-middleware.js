const ApiError = require('../exceptions/api-error');
const UserModel = require('../models/User');

module.exports = function (req, res, next) {
    const { userId } = req.body();

    if (err instanceof ApiError) {
        return res.status(err.status).json({ message: err.message, errors: err.errors });
    }
    
    return res.status(500).json({ message: 'Непредвиденная ошибка' });

};