const QRCode = require('qrcode');
const speakeasy = require('speakeasy');
const UserModel = require('../models/User');
const ApiError = require('../exceptions/api-error');

class AuthService {
    async generateQrCode(userId) {
        const secret = speakeasy.generateSecret({ name: '2FA-Auth' });
        const user = await UserModel.findById(userId);

        if (!user) {
            throw ApiError.BadRequest('Пользователь не найден!')
        }

        user.secret = secret.base32;
        await user.save();

        return secret.otpauth_url;
    }

    async verifyAuthToken(token, userId) {
        const user = await UserModel.findById(userId);
        
        if (!user) {
            throw ApiError.BadRequest('Пользователь не найден!');
        }

        const userSecret = user.secret;

        const verified = speakeasy.totp.verify({
            secret: userSecret,
            encoding: 'base32',
            token,
        });

        if (!verified) {
            throw ApiError.BadRequest('Не верный код!')
        }

        if (!user.is2FA) {
            user.is2FA = true;
        }

        await user.save();

        return {verifyToken: userSecret, verified};
    }
}

module.exports = new AuthService();