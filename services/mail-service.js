const nodemailer = require('nodemailer');

class MailService {

    constructor() {
        this.transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: '2fauthentificationapp@gmail.com',
                pass: 'jioj xnab vltz jphd'
            }
        })
    }

    async sendActivationMail(to, link) {
        console.log(process.env.SMTP_USER, process.env.SMTP_PASSWORD);
        await this.transporter.sendMail({
            from: process.env.SMTP_USER,
            to,
            subject: 'Активация аккаунта на ' + process.env.API_URL,
            text: '',
            html:
                `
                    <div>
                        <h1>Для активации перейдите по ссылке</h1>
                        <a href="${link}">${link}</a>
                    </div>
                `
        })
    }
}

module.exports = new MailService();